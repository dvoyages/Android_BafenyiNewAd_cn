package com.bafenyi.newadlib.config;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.provider.Settings;

import androidx.core.content.ContextCompat;

import com.bytedance.msdk.api.TTAdConfig;
import com.bytedance.msdk.api.TTAdConstant;
import com.bytedance.msdk.api.TTMediationAdSdk;
import com.bytedance.msdk.api.TTPrivacyConfig;
import com.bytedance.msdk.api.UserInfoForSegment;
import com.bytedance.msdk.api.v2.GMAdConfig;
import com.bytedance.msdk.api.v2.GMAdConstant;
import com.bytedance.msdk.api.v2.GMMediationAdSdk;
import com.bytedance.msdk.api.v2.GMPangleOption;
import com.bytedance.msdk.api.v2.GMPrivacyConfig;
import com.bytedance.msdk.api.v2.GMSettingConfigCallback;

import java.util.HashMap;
import java.util.Map;

import static android.provider.Settings.System.ANDROID_ID;


/**
 * 可以用一个单例来保存TTAdManager实例，在需要初始化sdk的时候调用
 */
public class TTAdManagerHolder {

    private static boolean sInit;  //是否已经初始化

    /**
     * 初始化groMore
     *
     * @param context     上下文
     * @param adId        广告ID
     * @param adAppName   广告名称
     * @param isDebugMode 是否是debug模式
     */
    public static void init(Context context, String adId, String adAppName, boolean isDebugMode) {
        doInit(context, adId, adAppName, isDebugMode);
    }


    //step1:接入网盟广告sdk的初始化操作，详情见接入文档和穿山甲平台说明
    private static void doInit(Context context, String adId, String adAppName, boolean isDebugMode) {
        if (!sInit) {
            GMMediationAdSdk.initialize(context, buildConfig(context, adId, adAppName, isDebugMode));
            sInit = true;
        }
    }

    private static GMAdConfig buildConfig(final Context context, String adId, String adAppName, boolean isDebugMode) {
        return new GMAdConfig.Builder()
                .setAppId(adId) //必填 ，不能为空
                .setAppName(adAppName) //必填，不能为空
                .setOpenAdnTest(false)
                .setPublisherDid(getAndroidId(context)) //用户自定义device_id
                .setDebug(isDebugMode) //测试阶段打开，可以通过日志排查问题，上线时去除该调用
                .setPangleOption(new GMPangleOption.Builder()
                        .setIsPaid(false)
                        .setTitleBarTheme(GMAdConstant.TITLE_BAR_THEME_DARK)
                        .setAllowShowNotify(true)
                        .setAllowShowPageWhenScreenLock(true)
                        .setDirectDownloadNetworkType(GMAdConstant.NETWORK_STATE_WIFI,GMAdConstant.NETWORK_STATE_4G, GMAdConstant.NETWORK_STATE_3G)
                        .setIsUseTextureView(true)
                        .setNeedClearTaskReset()
                        .setKeywords("")
                        .build())
                .setPrivacyConfig(new GMPrivacyConfig() {
                    @Override
                    public boolean isCanUseLocation() {
                        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return false;
                        } else {
                            return true;
                        }
                    }

                    @Override
                    public boolean isCanUsePhoneState() {
                        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            return false;
                        } else {
                            return true;
                        }
                    }

                    @Override
                    public boolean isCanUseWriteExternal() {
                        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                            return false;
                        } else {
                            return true;
                        }
                    }
                })
                .build();
    }

    public static String getAndroidId(Context context) {
        String androidId = null;
        try {
            androidId = Settings.System.getString(context.getContentResolver(), ANDROID_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return androidId;
    }

}
