/*
 * Copyright © 2019 XiaMen BaFenYi Network Technology Co., Ltd. All rights reserved.
 * 版权：厦门八分仪网络科技有限公司版权所有（C）2019
 * 作者：Administrator
 * 创建日期：2020年4月23日
 */

package com.bafenyi.newadlib.groMoreAd;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;

import com.bafenyi.newadlib.BFYNewAdMethod;
import com.bafenyi.newadlib.impl.InterstitialCallback;
import com.bafenyi.newadlib.util.AppConst;
import com.bafenyi.newadlib.util.VideoOptionUtil;
import com.bytedance.msdk.adapter.util.Logger;
import com.bytedance.msdk.api.AdError;
import com.bytedance.msdk.api.AdSlot;
import com.bytedance.msdk.api.TTMediationAdSdk;
import com.bytedance.msdk.api.TTSettingConfigCallback;
import com.bytedance.msdk.api.TTVideoOption;
import com.bytedance.msdk.api.interstitial.TTInterstitialAd;
import com.bytedance.msdk.api.interstitial.TTInterstitialAdListener;
import com.bytedance.msdk.api.interstitial.TTInterstitialAdLoadCallback;
import com.bytedance.msdk.api.v2.GMMediationAdSdk;
import com.bytedance.msdk.api.v2.GMSettingConfigCallback;
import com.bytedance.msdk.api.v2.ad.interstitial.GMInterstitialAd;
import com.bytedance.msdk.api.v2.ad.interstitial.GMInterstitialAdListener;
import com.bytedance.msdk.api.v2.ad.interstitial.GMInterstitialAdLoadCallback;
import com.bytedance.msdk.api.v2.slot.GMAdOptionUtil;
import com.bytedance.msdk.api.v2.slot.GMAdSlotInterstitial;

/**
 * 穿山甲插屏广告
 */
public class GroInterstitialADUtil {

    private static final String TAG = "GroInterstitialADUtil";
    private static GMInterstitialAd mInterstitialAd;
    private static  String mAdUnitId;
    @SuppressLint("StaticFieldLeak")
    private static Activity mActivity;
    private static InterstitialCallback mInterstitialCallback;
    private static int mWidth=300;
    private static int mHeight=450;
    /**
     * 判断当前是否存在config 配置 ，如果存在直接加载广告 ，如果不存在则注册config加载回调
     */
    public static void showInterstitialAdWithCallBack(Activity activity, String adUnitId,int weight,int height ,InterstitialCallback interstitialCallback){
        mAdUnitId=adUnitId;
        mActivity=activity;
        if(weight!=0&&height!=0){
            mWidth=weight;
            mHeight=height;
        }
        mInterstitialCallback=interstitialCallback;
        /**
         * 判断当前是否存在config 配置 ，如果存在直接加载广告 ，如果不存在则注册config加载回调
         */
        if (GMMediationAdSdk.configLoadSuccess()) {
            Log.e(AppConst.TAG, "load ad 当前config配置存在，直接加载广告");
            loadInteractionAd(activity,adUnitId,mWidth,mHeight,interstitialCallback);
        } else {
            Log.e(AppConst.TAG, "load ad 当前config配置不存在，正在请求config配置....");
            GMMediationAdSdk.registerConfigCallback(mSettingConfigCallback); //不能使用内部类，否则在ondestory中无法移除该回调
        }
    }


    private static GMSettingConfigCallback mSettingConfigCallback = new GMSettingConfigCallback() {
        @Override
        public void configLoad() {
            Log.e(TAG, "load ad 在config 回调中加载广告");
            loadInteractionAd(mActivity,mAdUnitId,mWidth,mHeight,mInterstitialCallback);
        }
    };

    public static void unregisterConfigCallback(){
        if(mSettingConfigCallback!=null){
            GMMediationAdSdk.unregisterConfigCallback(mSettingConfigCallback);
        }
    }

    /**
     * 加载插屏广告
     */
    private static void loadInteractionAd(final Activity activity,String adUnitId,int weight,int height,final InterstitialCallback interstitialCallback) {
        //Context 必须传activity
        /**
         * 注：每次加载插屏广告的时候需要新建一个GMInterstitialAd，否则可能会出现广告填充问题
         * （ 例如：mInterstitialAd = new GMInterstitialAd(this, adUnitId);）
         */
        mInterstitialAd = new GMInterstitialAd(activity, mAdUnitId);

        GMAdSlotInterstitial adSlotInterstitial = new GMAdSlotInterstitial.Builder()
                .setGMAdSlotBaiduOption(GMAdOptionUtil.getGMAdSlotBaiduOption().build())
                .setGMAdSlotGDTOption(GMAdOptionUtil.getGMAdSlotGDTOption().build())
                .setImageAdSize(height, weight)
                .setDownloadType(BFYNewAdMethod.download_type_popup)
                .build();

        //请求广告，调用插屏广告异步请求接口
        mInterstitialAd.loadAd(adSlotInterstitial, new GMInterstitialAdLoadCallback() {
            @Override
            public void onInterstitialLoadFail(AdError adError) {
                Log.e(TAG, "load interaction ad error : " + adError.code + ", " + adError.message);
                // 获取本次waterfall加载中，加载失败的adn错误信息。
                if (mInterstitialAd != null){
                    Log.d(TAG, "ad load infos: " + mInterstitialAd.getAdLoadInfoList());
                }
                interstitialCallback.onInterstitialError( adError.code,adError.message);
            }

            @Override
            public void onInterstitialLoad() {
                Log.e(TAG, "load interaction ad success ! ");
                // 获取本次waterfall加载中，加载失败的adn错误信息。
                if (mInterstitialAd != null){
                    Log.d(TAG, "ad load infos: " + mInterstitialAd.getAdLoadInfoList());
                    if ( mInterstitialAd != null && mInterstitialAd.isReady()) {
                        //设置监听器
                        mInterstitialAd.setAdInterstitialListener(setAdListener(interstitialCallback));
                        mInterstitialAd.showAd(activity);
                        Logger.e(AppConst.TAG, "adNetworkPlatformId: " + mInterstitialAd.getAdNetworkPlatformId() + "   adNetworkRitId：" + mInterstitialAd.getAdNetworkRitId() + "   preEcpm: " + mInterstitialAd.getPreEcpm());
                    }
                }
            }
        });

    }

    private static GMInterstitialAdListener setAdListener(final InterstitialCallback interstitialCallback){
        return new GMInterstitialAdListener() {

            /**
             * 广告展示
             */
            @Override
            public void onInterstitialShow() {
                interstitialCallback.onInterstitialSuccess(mInterstitialAd.getPreEcpm());
                Log.d(TAG, "onInterstitialShow");
            }

            @Override
            public void onInterstitialShowFail(AdError adError) {
                interstitialCallback.onInterstitialError(adError.code,adError.message);
            }


            /**
             * 广告被点击
             */
            @Override
            public void onInterstitialAdClick() {
                Log.d(TAG, "onInterstitialAdClick");
            }

            /**
             * 关闭广告
             */
            @Override
            public void onInterstitialClosed() {
                interstitialCallback.onInterstitialClose();
                Log.d(TAG, "onInterstitialClosed");
            }


            /**
             * 当广告打开浮层时调用，如打开内置浏览器、内容展示浮层，一般发生在点击之后
             * 常常在onAdLeftApplication之前调用
             */
            @Override
            public void onAdOpened() {
                Log.d(TAG, "onAdOpened");
            }


            /**
             * 此方法会在用户点击打开其他应用（例如 Google Play）时
             * 于 onAdOpened() 之后调用，从而在后台运行当前应用。
             */
            @Override
            public void onAdLeftApplication() {
                Log.d(TAG, "onAdLeftApplication");
            }
        };

    }


    /**
     * 销毁广告，在Activity销毁时调用
     */
    public static void DestroyAd(){
        unregisterConfigCallback();
        if(mInterstitialAd!=null)mInterstitialAd.destroy();
    }


}