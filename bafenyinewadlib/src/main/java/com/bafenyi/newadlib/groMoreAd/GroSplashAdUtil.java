package com.bafenyi.newadlib.groMoreAd;

import android.app.Activity;
import android.util.Log;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.bafenyi.newadlib.BFYNewAdMethod;
import com.bafenyi.newadlib.impl.SplashAdCallBack;
import com.bafenyi.newadlib.util.AppConst;
import com.bytedance.msdk.adapter.util.Logger;
import com.bytedance.msdk.api.AdError;
import com.bytedance.msdk.api.AdSlot;
import com.bytedance.msdk.api.NetworkPlatformConst;

import com.bytedance.msdk.api.v2.ad.splash.GMSplashAd;
import com.bytedance.msdk.api.v2.ad.splash.GMSplashAdListener;
import com.bytedance.msdk.api.v2.ad.splash.GMSplashAdLoadCallback;
import com.bytedance.msdk.api.v2.slot.GMAdSlotSplash;

public class GroSplashAdUtil {


    private static GMSplashAd mTTSplashAd;
    //是否是穿山甲广告
    private static boolean isTTSplashAd = false;
    private static final String TAG = "SplashAd";
    //加载超时时长
    private static final int AD_TIME_OUT = 4000;

    /**
     * 加载开屏广告
     *
     * @param activity
     * @param mSplashContainer
     * @param splashAdCallBack
     */
    public static void showSplashAd(@NonNull final Activity activity, @NonNull final String adUnitId, @NonNull final ViewGroup mSplashContainer, @NonNull final SplashAdCallBack splashAdCallBack) {
        /**
         * 注：每次加载开屏广告的时候需要新建一个TTSplashAd，否则可能会出现广告填充问题
         * （ 例如：mTTSplashAd = new TTSplashAd(this, mAdUnitId);）
         */
        mTTSplashAd = new GMSplashAd(activity, adUnitId);
        GMSplashAdListener mSplashAdListener = new GMSplashAdListener() {
            @Override
            public void onAdClicked() {
                Log.d(TAG, "onAdClicked");
                splashAdCallBack.OnClick();
            }

            @Override
            public void onAdShow() {
                Log.d(TAG, "onAdShow");
                isTTSplashAd = mTTSplashAd.getAdNetworkPlatformId() == NetworkPlatformConst.SDK_NAME_PANGLE;
                splashAdCallBack.OnShow(isTTSplashAd, mTTSplashAd.getPreEcpm());
            }

            @Override
            public void onAdShowFail(AdError adError) {
                splashAdCallBack.OnError(true, adError.message, adError.code);
            }

            @Override
            public void onAdSkip() {
                Log.d(TAG, "onAdSkip");
                splashAdCallBack.skipNextPager();
            }

            @Override
            public void onAdDismiss() {
                Log.d(TAG, "onAdDismiss");
                splashAdCallBack.skipNextPager();
            }
        };
        mTTSplashAd.setAdSplashListener(mSplashAdListener);

//step3:创建开屏广告请求参数AdSlot,具体参数含义参考文档
        GMAdSlotSplash adSlot = new GMAdSlotSplash.Builder()
                .setImageAdSize(1080, 1920)
                .setSplashPreLoad(true)//开屏gdt开屏广告预加载
                .setMuted(false) //声音开启
                .setVolume(1f)//admob 声音配置，与setMuted配合使用
                .setTimeOut(AD_TIME_OUT)//设置超时
                .setSplashButtonType(BFYNewAdMethod.download_button)
                .setDownloadType(BFYNewAdMethod.download_type_popup)
                .build();
        //自定义兜底方案 选择使用
//        TTNetworkRequestInfo ttNetworkRequestInfo;
//        //穿山甲兜底，参数分别是appId和adn代码位。注意第二个参数是代码位，而不是广告位。
//        ttNetworkRequestInfo = new PangleNetworkRequestInfo("5169402", "887523629");
        //gdt兜底
//        ttNetworkRequestInfo = new GdtNetworkRequestInfo("1101152570", "8863364436303842593");
        //ks兜底
//        ttNetworkRequestInfo = new KsNetworkRequestInfo("90009", "4000000042");
        //百度兜底
//        ttNetworkRequestInfo = new BaiduNetworkRequestInfo("e866cfb0", "2058622");

        //step4:请求广告，调用开屏广告异步请求接口，对请求回调的广告作渲染处理
        mTTSplashAd.loadAd(adSlot, new GMSplashAdLoadCallback() {
            @Override
            public void onSplashAdLoadFail(AdError adError) {
                Log.d(TAG, adError.message);
                Log.e(TAG, "load splash ad error : " + adError.code + ", " + adError.message);
                // 获取本次waterfall加载中，加载失败的adn错误信息。
                if (mTTSplashAd != null) {
                    Log.d(TAG, "ad load infos: " + mTTSplashAd.getAdLoadInfoList());
                }
                splashAdCallBack.OnError(true, adError.message, adError.code);
                splashAdCallBack.skipNextPager();
            }

            @Override
            public void onSplashAdLoadSuccess() {
                if (mTTSplashAd != null) {
                    mTTSplashAd.showAd(mSplashContainer);
                    Logger.e(AppConst.TAG, "adNetworkPlatformId: " + mTTSplashAd.getAdNetworkPlatformId() + "   adNetworkRitId：" + mTTSplashAd.getAdNetworkRitId() + "   preEcpm: " + mTTSplashAd.getPreEcpm());
                    // 获取本次waterfall加载中，加载失败的adn错误信息。
                    if (mTTSplashAd != null) {
                        Log.d(TAG, "ad load infos: " + mTTSplashAd.getAdLoadInfoList());
                    }
                }
                Log.e(TAG, "load splash ad success ");
            }

            @Override
            public void onAdLoadTimeout() {
                Log.i(TAG, "开屏广告加载超时.......");
                // 获取本次waterfall加载中，加载失败的adn错误信息。
                if (mTTSplashAd != null) {
                    Log.d(TAG, "ad load infos: " + mTTSplashAd.getAdLoadInfoList());
                }
                splashAdCallBack.skipNextPager();
            }
        });
    }

    /**
     * 销毁广告，在Activity销毁时调用
     */
    public static void DestroyAd() {
        if (mTTSplashAd != null) mTTSplashAd.destroy();
    }

}
