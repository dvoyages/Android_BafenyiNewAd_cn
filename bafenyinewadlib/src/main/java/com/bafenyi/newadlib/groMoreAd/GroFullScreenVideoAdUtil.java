package com.bafenyi.newadlib.groMoreAd;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.bafenyi.newadlib.BFYNewAdMethod;
import com.bafenyi.newadlib.impl.FullScreenVideoAdCallback;
import com.bafenyi.newadlib.util.AppConst;
import com.bafenyi.newadlib.util.VideoOptionUtil;
import com.bytedance.msdk.adapter.util.Logger;
import com.bytedance.msdk.api.AdError;
import com.bytedance.msdk.api.AdSlot;
import com.bytedance.msdk.api.TTMediationAdSdk;
import com.bytedance.msdk.api.TTSettingConfigCallback;
import com.bytedance.msdk.api.TTVideoOption;
import com.bytedance.msdk.api.fullVideo.TTFullVideoAd;
import com.bytedance.msdk.api.fullVideo.TTFullVideoAdListener;
import com.bytedance.msdk.api.fullVideo.TTFullVideoAdLoadCallback;
import com.bytedance.msdk.api.reward.RewardItem;
import com.bytedance.msdk.api.v2.GMAdConstant;
import com.bytedance.msdk.api.v2.GMMediationAdSdk;
import com.bytedance.msdk.api.v2.GMSettingConfigCallback;
import com.bytedance.msdk.api.v2.ad.fullvideo.GMFullVideoAd;
import com.bytedance.msdk.api.v2.ad.fullvideo.GMFullVideoAdListener;
import com.bytedance.msdk.api.v2.ad.fullvideo.GMFullVideoAdLoadCallback;
import com.bytedance.msdk.api.v2.ad.interstitialFull.GMInterstitialFullAd;
import com.bytedance.msdk.api.v2.ad.interstitialFull.GMInterstitialFullAdListener;
import com.bytedance.msdk.api.v2.ad.interstitialFull.GMInterstitialFullAdLoadCallback;
import com.bytedance.msdk.api.v2.slot.GMAdOptionUtil;
import com.bytedance.msdk.api.v2.slot.GMAdSlotFullVideo;
import com.bytedance.msdk.api.v2.slot.GMAdSlotInterstitialFull;
import com.bytedance.sdk.openadsdk.TTAdConstant;

import java.util.HashMap;
import java.util.Map;

import static android.widget.Toast.LENGTH_SHORT;

public class GroFullScreenVideoAdUtil {
    public static boolean show=false;
    /**
     * 视频缓存是否加载完成
     */
    public static boolean isLoad=false;
    public static boolean isLoading=false;

    private static GMInterstitialFullAdListener mGMInterstitialFullAdListener = new GMInterstitialFullAdListener() {
        @Override
        public void onInterstitialFullShow() {
             mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mGMInterstitialFullAd != null)
                        mFullScreenVideoAdCallback.onRewardSuccessShow(mGMInterstitialFullAd.getShowEcpm().getPreEcpm());
                }
            });
        }

        @Override
        public void onInterstitialFullShowFail(@NonNull AdError adError) {

        }

        @Override
        public void onInterstitialFullClick() {

        }

        @Override
        public void onInterstitialFullClosed() {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mFullScreenVideoAdCallback.OnClose();
                }
            });
        }

        @Override
        public void onVideoComplete() {

        }

        @Override
        public void onVideoError() {

        }

        @Override
        public void onSkippedVideo() {

        }

        @Override
        public void onAdOpened() {

        }

        @Override
        public void onAdLeftApplication() {

        }

        @Override
        public void onRewardVerify(@NonNull RewardItem rewardItem) {

        }
    };


    private static GMInterstitialFullAd mGMInterstitialFullAd;
    private static GMInterstitialFullAdLoadCallback mGMInterstitialFullAdLoadCallback = new GMInterstitialFullAdLoadCallback() {
        @Override
        public void onInterstitialFullLoadFail(@NonNull AdError adError) {
            isLoad=false;
            isLoading=false;
            mFullScreenVideoAdCallback.error(true, adError.message, adError.code);
        }

        @Override
        public void onInterstitialFullAdLoad() {
        }

        @Override
        public void onInterstitialFullCached() {
            if (mGMInterstitialFullAd != null) {
                isLoad=false;
                isLoading=false;
                show=false;
                //展示广告，并传入广告展示的场景
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mGMInterstitialFullAd.setAdInterstitialFullListener(mGMInterstitialFullAdListener);
                        mGMInterstitialFullAd.showAd(mActivity);
                    }
                });
            } else {
                Toast.makeText(mActivity,"请先加载广告", LENGTH_SHORT);
            }
        }
    };

    private static String mAdUnitId;
    @SuppressLint("StaticFieldLeak")
    private static Activity mActivity;
    private static final String TAG = "GroFullScreenVideoAdUti";
    private static FullScreenVideoAdCallback mFullScreenVideoAdCallback;



    public static void showAdWithCallback(Activity activity, String adUnitId, FullScreenVideoAdCallback fullScreenVideoAdCallback) {
        mActivity = activity;
        mAdUnitId = adUnitId;
        show=true;
        if(isLoading)return;
        isLoading=true;
        isLoad=false;
        mFullScreenVideoAdCallback = fullScreenVideoAdCallback;
        /**
         * 判断当前是否存在config 配置 ，如果存在直接加载广告 ，如果不存在则注册config加载回调
         */
        if (GMMediationAdSdk.configLoadSuccess()) {
            Log.e(AppConst.TAG, "load ad 当前config配置存在，直接加载广告");
            loadAd(activity, adUnitId, fullScreenVideoAdCallback);
        } else {
            Log.e(AppConst.TAG, "load ad 当前config配置不存在，正在请求config配置....");
            GMMediationAdSdk.registerConfigCallback(mSettingConfigCallback); //不能使用内部类，否则在ondestory中无法移除该回调
        }

    }

    /**
     * config回调
     */
    private static GMSettingConfigCallback mSettingConfigCallback = new GMSettingConfigCallback() {
        @Override
        public void configLoad() {
            Log.e(TAG, "load ad 在config 回调中加载广告");
            loadAd(mActivity, mAdUnitId, mFullScreenVideoAdCallback);
        }
    };

    /**
     * config回调取消，在页面销毁时调用
     */
    public static void UnregisterConfigCallback() {
        if (mSettingConfigCallback != null)
            GMMediationAdSdk.unregisterConfigCallback(mSettingConfigCallback);
    }

    private static void loadAd(final Activity activity, String adUnitId, final FullScreenVideoAdCallback fullScreenVideoAdCallback) {
        /**
         * 注：每次加载插全屏广告的时候需要新建一个GMInterstitialFullAd，否则可能会出现广告填充问题
         * （ 例如：mInterstitialFullAd = new GMInterstitialFullAd(this, adUnitId);）
         */
        //Context 必须传activity
        mGMInterstitialFullAd = new GMInterstitialFullAd(mActivity, adUnitId);

        Map<String, String> customData = new HashMap<>();
        customData.put(GMAdConstant.CUSTOM_DATA_KEY_GDT, "gdt custom data");//目前仅支持gdt
        // 其他需要透传给adn的数据。

        GMAdSlotInterstitialFull adSlotInterstitialFull = new GMAdSlotInterstitialFull.Builder()
                .setGMAdSlotBaiduOption(GMAdOptionUtil.getGMAdSlotBaiduOption().build())
                .setGMAdSlotGDTOption(GMAdOptionUtil.getGMAdSlotGDTOption().build())
                .setImageAdSize(600, 600)  //设置宽高 （插全屏类型下_插屏广告使用）
                .setVolume(0.5f) //admob 声音配置，与setMuted配合使用
                .setUserID("user123")//用户id,必传参数 (插全屏类型下_全屏广告使用)
                .setCustomData(customData)
                .setRewardName("金币") //奖励的名称
                .setRewardAmount(3)  //奖励的数量
                .setOrientation(GMAdConstant.HORIZONTAL)//必填参数，期望视频的播放方向：TTAdConstant.HORIZONTAL 或 TTAdConstant.VERTICAL; (插全屏类型下_全屏广告使用)
                .build();

        //请求广告，调用插屏广告异步请求接口
        mGMInterstitialFullAd.loadAd(adSlotInterstitialFull, mGMInterstitialFullAdLoadCallback);
    }


    /**
     * 销毁广告，在Activity销毁时调用
     */
    public static void DestroyAd() {
        UnregisterConfigCallback();
        if (mGMInterstitialFullAd != null) mGMInterstitialFullAd.destroy();
    }

}
