package com.bafenyi.newadlib.groMoreAd;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.bafenyi.newadlib.BFYNewAdMethod;
import com.bafenyi.newadlib.impl.RewardVideoAdCallBack;
import com.bafenyi.newadlib.util.AppConst;
import com.bytedance.msdk.adapter.util.Logger;
import com.bytedance.msdk.api.AdError;
import com.bytedance.msdk.api.AdSlot;
import com.bytedance.msdk.api.NetworkPlatformConst;
import com.bytedance.msdk.api.TTMediationAdSdk;
import com.bytedance.msdk.api.TTSettingConfigCallback;
import com.bytedance.msdk.api.TTVideoOption;
import com.bytedance.msdk.api.reward.RewardItem;
import com.bytedance.msdk.api.reward.TTRewardAd;
import com.bytedance.msdk.api.reward.TTRewardedAdListener;
import com.bytedance.msdk.api.reward.TTRewardedAdLoadCallback;
import com.bytedance.msdk.api.v2.GMAdConstant;
import com.bytedance.msdk.api.v2.GMMediationAdSdk;
import com.bytedance.msdk.api.v2.GMSettingConfigCallback;
import com.bytedance.msdk.api.v2.ad.reward.GMRewardAd;
import com.bytedance.msdk.api.v2.ad.reward.GMRewardedAdListener;
import com.bytedance.msdk.api.v2.slot.GMAdOptionUtil;
import com.bytedance.msdk.api.v2.slot.GMAdSlotRewardVideo;

import java.util.HashMap;
import java.util.Map;

public class GroRewardVideoAdUtil {

    private static GMRewardAd mttRewardAd;
    private static final String TAG = "RewardVideoAd";
    @SuppressLint("StaticFieldLeak")
    private static Activity mContext;
    private static String adUnitId;  //激励广告id
    private static RewardVideoAdCallBack rewardVideoAdCallBack;
    //是否是穿山甲广告
    private static boolean isTTSplashAd = false;
    //是否是穿山甲
    private static boolean isRewardVerify;
    private static int mOrientation;

    /**
     * 展示激励视频广告
     *
     * @param context
     * @param adUnitId
     * @param mRewardVideoAdCallBack
     */
    public static void showAdWithCallback(@NonNull final Activity context,
                                          @NonNull final String adUnitId,
                                          int orientation,
                                          @NonNull final RewardVideoAdCallBack mRewardVideoAdCallBack) {
        mOrientation = orientation;
        mContext = context;
        rewardVideoAdCallBack = mRewardVideoAdCallBack;
        isRewardVerify = false;


        /**
         * 判断当前是否存在config 配置 ，如果存在直接加载广告 ，如果不存在则注册config加载回调
         */
        if (GMMediationAdSdk.configLoadSuccess()) {
            Log.e(AppConst.TAG, "load ad 当前config配置存在，直接加载广告");
            loadAd(context, adUnitId, orientation, rewardVideoAdCallBack);
        } else {
            Log.e(AppConst.TAG, "load ad 当前config配置不存在，正在请求config配置....");
            GMMediationAdSdk.registerConfigCallback(mSettingConfigCallback); //不能使用内部类，否则在ondestory中无法移除该回调
        }

    }

    /**
     * config回调取消，在页面销毁时调用
     */
    public static void UnregisterConfigCallback() {
        if (mSettingConfigCallback != null)
            GMMediationAdSdk.unregisterConfigCallback(mSettingConfigCallback);
    }

    /**
     * config回调 ,加载广告
     */
    private static GMSettingConfigCallback mSettingConfigCallback = new GMSettingConfigCallback() {
        @Override
        public void configLoad() {
            Log.e(TAG, "load ad 在config 回调中加载广告");
            loadAd(mContext, adUnitId, mOrientation, rewardVideoAdCallBack);
        }
    };

    public static void loadAd(final Activity context, final String adUnitId, int orientation, final RewardVideoAdCallBack rewardVideoAdCallBack) {
        /**
         * 注：每次加载激励视频广告的时候需要新建一个TTRewardAd，否则可能会出现广告填充问题
         * （ 例如：mttRewardAd = new TTRewardAd(this, adUnitId);）
         */
        mttRewardAd = new GMRewardAd(context, adUnitId);


        Map<String, String> customData = new HashMap<>();
        customData.put(GMAdConstant.CUSTOM_DATA_KEY_PANGLE, "pangle media_extra");
        customData.put(GMAdConstant.CUSTOM_DATA_KEY_GDT, "gdt custom data");
        customData.put(GMAdConstant.CUSTOM_DATA_KEY_KS, "ks custom data");
        // 其他需要透传给adn的数据。

        GMAdSlotRewardVideo adSlotRewardVideo = new GMAdSlotRewardVideo.Builder()
                .setMuted(true)//对所有SDK的激励广告生效，除需要在平台配置的SDK，如穿山甲SDK
                .setVolume(0.5f)//配合Admob的声音大小设置[0-1]
                .setDownloadType(BFYNewAdMethod.download_type_popup)
                .setGMAdSlotGDTOption(GMAdOptionUtil.getGMAdSlotGDTOption().build())
                .setGMAdSlotBaiduOption(GMAdOptionUtil.getGMAdSlotBaiduOption().build())
                .setCustomData(customData)
                .setRewardName("金币") //奖励的名称
                .setRewardAmount(3)  //奖励的数量
                .setUserID("user123")//用户id,必传参数
                .setOrientation(orientation)//必填参数，期望视频的播放方向：GMAdConstant.HORIZONTAL 或 GMAdConstant.VERTICAL
                .build();
        //请求广告
        mttRewardAd.loadAd(adSlotRewardVideo, new TTRewardedAdLoadCallback() {

            @Override
            public void onRewardVideoLoadFail(AdError adError) {
                Log.e(TAG, "load RewardVideo ad error : " + adError.code + ", " + adError.message);
                // 获取本次waterfall加载中，加载失败的adn错误信息。
                if (mttRewardAd != null) {
                    Log.d(TAG, "reward ad loadinfos: " + mttRewardAd.getAdLoadInfoList());
                }
                rewardVideoAdCallBack.onErrorRewardVideo(true, adError.message, adError.code);

            }

            @Override
            public void onRewardVideoAdLoad() {
                Log.e(TAG, "load RewardVideo ad success !" + mttRewardAd.isReady());
                // 获取本次waterfall加载中，加载失败的adn错误信息。
                if (mttRewardAd != null) {
                    Log.d(TAG, "reward ad loadinfos: " + mttRewardAd.getAdLoadInfoList());
                }


            }

            @Override
            public void onRewardVideoCached() {
                Log.d(TAG, "onRewardVideoCached....缓存成功" + mttRewardAd.isReady());
                if (mttRewardAd != null) {
                    rewardVideoAdCallBack.onGetRewardVideo();
                    Log.d(TAG, "reward ad loadinfos: " + mttRewardAd.getAdLoadInfoList());
                    ShowRewardVideoAd(mttRewardAd, context, rewardVideoAdCallBack);
                }
            }
        });
    }

    /**
     * 加载完成展示广告
     *
     * @param mttRewardAd
     * @param context
     * @param rewardVideoAdCallBack
     */
    private static void ShowRewardVideoAd(final GMRewardAd mttRewardAd, final Activity context, final RewardVideoAdCallBack rewardVideoAdCallBack) {
        mttRewardAd.setRewardAdListener(new GMRewardedAdListener() {
            /**
             * 广告的展示回调 每个广告仅回调一次
             */
            public void onRewardedAdShow() {
                Log.d(TAG, "onRewardedAdShow");
                isTTSplashAd = mttRewardAd.getAdNetworkPlatformId() == NetworkPlatformConst.SDK_NAME_PANGLE;
                rewardVideoAdCallBack.onShowRewardVideo(isTTSplashAd, mttRewardAd.getPreEcpm());
            }

            /**
             * 无广告可用
             * @param adError
             */
            @Override
            public void onRewardedAdShowFail(AdError adError) {
                rewardVideoAdCallBack.onErrorRewardVideo(true, adError.message, adError.code);
            }

            /**
             * 注意Admob的激励视频不会回调该方法
             */
            @Override
            public void onRewardClick() {
                rewardVideoAdCallBack.onClickRewardVideo();
                Log.d(TAG, "onRewardClick");
            }

            /**
             * 广告关闭的回调
             */
            public void onRewardedAdClosed() {
                Log.d(TAG, "onRewardedAdClosed");
                rewardVideoAdCallBack.onCloseRewardVideo(isRewardVerify);
            }

            /**
             * 视频播放完毕的回调 Admob广告不存在该回调
             */
            public void onVideoComplete() {
                Log.d(TAG, "onVideoComplete");

            }

            /**
             * 1、视频播放失败的回调
             * 2、如果show时发现无可用广告（比如广告过期），也会回调。
             */
            public void onVideoError() {
                Log.d(TAG, "onVideoError");
                rewardVideoAdCallBack.onCloseRewardVideo(false);
            }

            /**
             * 激励视频播放完毕，验证是否有效发放奖励的回调
             */
            public void onRewardVerify(RewardItem rewardItem) {
                Map<String, Object> customData = rewardItem.getCustomData();
                if (customData != null) {
                    String adnName = (String) customData.get(RewardItem.KEY_ADN_NAME);
                    switch (adnName) {
                        case RewardItem.KEY_GDT:
                            isRewardVerify = true;
                            rewardVideoAdCallBack.onGetReward(rewardItem.rewardVerify(), rewardItem.getAmount(), rewardItem.getRewardName(), rewardItem.getCustomData());
                            Logger.d(TAG, "rewardItem gdt: " + customData.get(RewardItem.KEY_GDT_TRANS_ID));
                            break;
                    }
                }
                Log.d(TAG, "onRewardVerify");
                isRewardVerify = true;
            }

            /**
             * - Mintegral GDT Admob广告不存在该回调
             */
            @Override
            public void onSkippedVideo() {

            }

        });
        mttRewardAd.showRewardAd(context);
    }

    /**
     * 销毁广告，在Activity销毁时调用
     */
    public static void DestroyAd() {
        UnregisterConfigCallback();
        if (mttRewardAd != null) mttRewardAd.destroy();
    }

}
