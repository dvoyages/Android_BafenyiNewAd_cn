package com.bafenyi.newadlib.groMoreAd;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;

import com.bafenyi.newadlib.BFYNewAdMethod;
import com.bafenyi.newadlib.impl.BannerAdCallback;
import com.bafenyi.newadlib.util.AppConst;
import com.bytedance.msdk.adapter.util.Logger;
import com.bytedance.msdk.api.AdError;
import com.bytedance.msdk.api.AdSlot;
import com.bytedance.msdk.api.TTAdSize;
import com.bytedance.msdk.api.TTMediationAdSdk;
import com.bytedance.msdk.api.TTSettingConfigCallback;
import com.bytedance.msdk.api.banner.TTAdBannerListener;
import com.bytedance.msdk.api.banner.TTAdBannerLoadCallBack;
import com.bytedance.msdk.api.banner.TTBannerViewAd;
import com.bytedance.msdk.api.v2.GMAdSize;
import com.bytedance.msdk.api.v2.GMMediationAdSdk;
import com.bytedance.msdk.api.v2.GMSettingConfigCallback;
import com.bytedance.msdk.api.v2.ad.banner.GMBannerAd;
import com.bytedance.msdk.api.v2.ad.banner.GMBannerAdListener;
import com.bytedance.msdk.api.v2.ad.banner.GMBannerAdLoadCallback;
import com.bytedance.msdk.api.v2.slot.GMAdSlotBanner;

public class GroBannerAdUtil {


    private static final String TAG = "BannerAd";
    private static GMBannerAd mTTBannerViewAd;
    private static String mAdUnitId;  //广告ID
    @SuppressLint("StaticFieldLeak")
    private static Context mContext;
    private static BannerAdCallback bannerAdCallback;
    @SuppressLint("StaticFieldLeak")
    private static ViewGroup mBannerContainer;
    private static int width=320;
    private static int height=150;
    private static int sizeType;
    /**
     *
     * @param context
     * @param adUnitId
     * @param mSizeType
     * @param bannerContainer
     * @param mBannerAdCallback
     */
    public static void showBannerAdWithCallback(Context context, String adUnitId,  ViewGroup bannerContainer,int mSizeType, BannerAdCallback mBannerAdCallback) {
        width=320;
        height=150;
        sizeType=mSizeType;
        mContext=context;
        bannerAdCallback=mBannerAdCallback;
        mBannerContainer=bannerContainer;
        mAdUnitId=adUnitId;

        /**
         * 判断当前是否存在config 配置 ，如果存在直接加载广告 ，如果不存在则注册config加载回调
         */
        if (GMMediationAdSdk.configLoadSuccess()) {
            Log.e(AppConst.TAG, "load ad 当前config配置存在，直接加载广告");
            loadBannerAd(context,mAdUnitId,bannerContainer,mBannerAdCallback);
        } else {
            Log.e(AppConst.TAG, "load ad 当前config配置不存在，正在请求config配置....");
            GMMediationAdSdk.registerConfigCallback(mSettingConfigCallback); //不能使用内部类，否则在ondestory中无法移除该回调
        }
    }


    /**
     *
     * @param context
     * @param adUnitId
     * @param bannerContainer
     * @param mWidth
     * @param mHeight
     * @param mBannerAdCallback
     */
    public static void showBannerAdWithCallback(Context context,String adUnitId, ViewGroup bannerContainer,int mWidth,int mHeight,BannerAdCallback mBannerAdCallback) {
        width=mWidth;
        height=mHeight;
        mContext=context;
        sizeType=TTAdSize.BANNER_CUSTOME;
        bannerAdCallback=mBannerAdCallback;
        mBannerContainer=bannerContainer;
        mAdUnitId=adUnitId;
        /**
         * 判断当前是否存在config 配置 ，如果存在直接加载广告 ，如果不存在则注册config加载回调
         */
        if (GMMediationAdSdk.configLoadSuccess()) {
            Log.e(AppConst.TAG, "load ad 当前config配置存在，直接加载广告");
            loadBannerAd(context,mAdUnitId,bannerContainer,mBannerAdCallback);
        } else {
            Log.e(AppConst.TAG, "load ad 当前config配置不存在，正在请求config配置....");
            GMMediationAdSdk.registerConfigCallback(mSettingConfigCallback); //不能使用内部类，否则在ondestory中无法移除该回调
        }

    }

    public static void UnregisterConfigCallback(){
        if(mSettingConfigCallback!=null)
            GMMediationAdSdk.unregisterConfigCallback(mSettingConfigCallback);
    }
    /**
     * config回调
     */
    private static GMSettingConfigCallback mSettingConfigCallback = new GMSettingConfigCallback() {
        @Override
        public void configLoad() {
            Log.e(TAG, "load ad 在config 回调中加载广告");
            loadBannerAd(mContext,mAdUnitId,mBannerContainer,bannerAdCallback);
        }
    };


    private static void loadBannerAd(Context context, String mAdUnitId, ViewGroup bannerContainer,final BannerAdCallback mBannerAdCallback) {
        /**
         * 注：每次加载banner的时候需要新建一个TTBannerViewAd，否则可能会出现广告填充问题
         * （ 例如：mTTBannerViewAd = new TTBannerViewAd(this, adUnitId);）
         */
        //step4:创建广告请求参数AdSlot,具体参数含义参考文档
        mTTBannerViewAd = new GMBannerAd((Activity) context, mAdUnitId);
        mTTBannerViewAd.setAdBannerListener(SetBannerListener(context ,mBannerAdCallback));
        //step4:创建广告请求参数AdSlot,具体参数含义参考文档

        GMAdSlotBanner slotBanner = new GMAdSlotBanner.Builder()
                .setBannerSize(sizeType)
                .setImageAdSize(width, height)// GMAdSize.BANNER_CUSTOME可以调用setImageAdSize设置大小
                .setDownloadType(BFYNewAdMethod.download_type_popup)
                .setAllowShowCloseBtn(true)//如果广告本身允许展示关闭按钮，这里设置为true就是展示。注：目前只有mintegral支持。
                .build();
        //step5:请求广告，对请求回调的广告作渲染处理
        mTTBannerViewAd.loadAd(slotBanner, new GMBannerAdLoadCallback() {
            @Override
            public void onAdFailedToLoad(@NonNull AdError adError) {
                Log.e(TAG, "load banner ad error : " + adError.code + ", " + adError.message);
                mBannerContainer.removeAllViews();
                // 获取本次waterfall加载中，加载失败的adn错误信息。
                if (mBannerAdCallback != null) {
                    mBannerAdCallback.onError( adError.code,adError.message);
                }
                if (mTTBannerViewAd != null) Log.d(TAG, "banner adLoadInfo:" + mTTBannerViewAd.getAdLoadInfoList().toString());

            }

            @Override
            public void onAdLoaded() {
                mBannerContainer.removeAllViews();
                if (mTTBannerViewAd != null) {
                    //横幅广告容器的尺寸必须至少与横幅广告一样大。如果您的容器留有内边距，实际上将会减小容器大小。如果容器无法容纳横幅广告，则横幅广告不会展示
                    View view = mTTBannerViewAd.getBannerView();
                    if (view != null)
                        mBannerContainer.addView(view);
                    Logger.e(AppConst.TAG, "adNetworkPlatformId: " + mTTBannerViewAd.getAdNetworkPlatformId() + "   adNetworkRitId：" + mTTBannerViewAd.getAdNetworkRitId() + "   preEcpm: " + mTTBannerViewAd.getPreEcpm());

                }
                Log.i(TAG, "banner load success ");
                // 获取本次waterfall加载中，加载失败的adn错误信息。
                if (mTTBannerViewAd != null) Log.d(TAG, "banner adLoadInfo:" + mTTBannerViewAd.getAdLoadInfoList().toString());

            }
        });


    }

    /**
     * 设置监听
     * @param context
     * @return
     */
    private static GMBannerAdListener SetBannerListener(final Context context, final BannerAdCallback mBannerAdCallback) {
        GMBannerAdListener ttAdBannerListener = new GMBannerAdListener() {

            @Override
            public void onAdOpened() {
                Log.d(TAG, "onAdOpened");

            }

            @Override
            public void onAdLeftApplication() {
                Log.d(TAG, "onAdLeftApplication");

            }

            @Override
            public void onAdClosed() {
                Log.d(TAG, "onAdClosed");
                if (mBannerAdCallback != null) {
                    mBannerAdCallback.onClose();
                }
            }

            @Override
            public void onAdClicked() {
                Log.d(TAG, "onAdClicked");
                if (mBannerAdCallback != null) {
                    mBannerAdCallback.onClick();
                }
            }

            @Override
            public void onAdShow() {
                Log.d(TAG, "onAdShow");
                if (mBannerAdCallback != null) {
                    mBannerAdCallback.onShow(mTTBannerViewAd.getPreEcpm());
                }
            }

            @Override
            public void onAdShowFail(AdError adError) {
                mBannerAdCallback.onError(adError.code,adError.message);
            }
        };
        return ttAdBannerListener;


    }

    /**
     * 销毁广告，在Activity销毁时调用
     */
    public static void DestroyAd(){
        UnregisterConfigCallback();
        if(mTTBannerViewAd!=null)mTTBannerViewAd.destroy();
    }


}
