/*
 * Copyright © 2019 XiaMen BaFenYi Network Technology Co., Ltd. All rights reserved.
 * 版权：厦门八分仪网络科技有限公司版权所有（C）2019
 * 作者：Administrator
 * 创建日期：2020年4月22日
 */

package com.bafenyi.newadlib.groMoreAd;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;

import com.bafenyi.newadlib.BFYNewAdMethod;
import com.bafenyi.newadlib.impl.NativeAdCallback;
import com.bafenyi.newadlib.util.AppConst;
import com.bafenyi.newadlib.util.UIUtils;
import com.bafenyi.newadlib.util.VideoOptionUtil;
import com.bytedance.msdk.api.AdError;
import com.bytedance.msdk.api.AdSlot;
import com.bytedance.msdk.api.AdmobNativeAdOptions;
import com.bytedance.msdk.api.TTAdSize;
import com.bytedance.msdk.api.TTDislikeCallback;
import com.bytedance.msdk.api.TTMediationAdSdk;
import com.bytedance.msdk.api.TTSettingConfigCallback;
import com.bytedance.msdk.api.TTVideoOption;
import com.bytedance.msdk.api.nativeAd.TTNativeAd;
import com.bytedance.msdk.api.nativeAd.TTNativeAdLoadCallback;
import com.bytedance.msdk.api.nativeAd.TTNativeExpressAdListener;
import com.bytedance.msdk.api.nativeAd.TTUnifiedNativeAd;
import com.bytedance.msdk.api.nativeAd.TTVideoListener;
import com.bytedance.msdk.api.v2.GMMediationAdSdk;
import com.bytedance.msdk.api.v2.GMSettingConfigCallback;
import com.bytedance.msdk.api.v2.ad.nativeAd.GMNativeAd;
import com.bytedance.msdk.api.v2.ad.nativeAd.GMNativeAdListener;
import com.bytedance.msdk.api.v2.ad.nativeAd.GMNativeAdLoadCallback;
import com.bytedance.msdk.api.v2.ad.nativeAd.GMNativeExpressAdListener;
import com.bytedance.msdk.api.v2.ad.nativeAd.GMUnifiedNativeAd;
import com.bytedance.msdk.api.v2.ad.nativeAd.GMVideoListener;
import com.bytedance.msdk.api.v2.slot.GMAdOptionUtil;
import com.bytedance.msdk.api.v2.slot.GMAdSlotNative;
import com.bytedance.msdk.api.v2.slot.paltform.GMAdSlotGDTOption;

import java.util.List;

/**
 * 穿山甲原生广告（信息流广告）
 */
public class GroNativeAdUtil {

    private static GMUnifiedNativeAd mTTAdNative;
    private static String mAdUnitId;
    private static final String TAG = "GroNativeAdUtil";
    @SuppressLint("StaticFieldLeak")
    private static Activity mActivity;
    private static FrameLayout mViewGroup;
    private static NativeAdCallback mNativeAdCallback;
    private static int mDistance;

    public static void showNativeAdWithCallback(Activity activity, String adUnitId, @NonNull final FrameLayout viewGroup, int distance,NativeAdCallback nativeAdCallback) {
        mActivity=activity;
        mAdUnitId=adUnitId;
        mViewGroup=viewGroup;
        mNativeAdCallback=nativeAdCallback;
        mDistance=distance;
        /**
         * 判断当前是否存在config 配置 ，如果存在直接加载广告 ，如果不存在则注册config加载回调
         */
        if (GMMediationAdSdk.configLoadSuccess()) {
            Log.e(AppConst.TAG, "load ad 当前config配置存在，直接加载广告");
            loadListAd(activity,adUnitId,viewGroup,distance,nativeAdCallback);
        } else {
            Log.e(AppConst.TAG, "load ad 当前config配置不存在，正在请求config配置....");
            GMMediationAdSdk.registerConfigCallback(mSettingConfigCallback); //不能使用内部类，否则在ondestory中无法移除该回调
        }

    }


    public static void unregisterConfigCallback(){
        if(mSettingConfigCallback!=null){
            GMMediationAdSdk.unregisterConfigCallback(mSettingConfigCallback);
        }
    }


    /**
     * config回调
     */
    private static GMSettingConfigCallback mSettingConfigCallback = new GMSettingConfigCallback() {
        @Override
        public void configLoad() {
            Log.e(TAG, "load ad 在config 回调中加载广告");
            loadListAd(mActivity,mAdUnitId,mViewGroup,mDistance,mNativeAdCallback);
        }
    };

    /**
     * 加载feed广告
     */
    private static void loadListAd(final Activity activity,String adUnitId, @NonNull final FrameLayout viewGroup, int distance,final NativeAdCallback nativeAdCallback) {
        mTTAdNative = new GMUnifiedNativeAd(activity, adUnitId);//模板视频
        // 针对Admob Native的特殊配置项
        AdmobNativeAdOptions admobNativeAdOptions = new AdmobNativeAdOptions();
        admobNativeAdOptions.setAdChoicesPlacement(AdmobNativeAdOptions.ADCHOICES_TOP_RIGHT)//设置广告小标默认情况下，广告选择叠加层会显示在右上角。
                .setRequestMultipleImages(true)//素材中可能包含多张图片，如果此值设置为true， 则表示需要展示多张图片，此值设置为 false（默认），仅提供第一张图片。
                .setReturnUrlsForImageAssets(true);//设置为true，SDK会仅提供Uri字段的值，允许自行决定是否下载实际图片，同时不会提供Drawable字段的值

        // 针对Gdt Native自渲染广告，可以自定义gdt logo的布局参数。该参数可选,非必须。
        FrameLayout.LayoutParams gdtNativeAdLogoParams =
                new FrameLayout.LayoutParams(
                        UIUtils.dip2px(activity.getApplicationContext(), 40),
                        UIUtils.dip2px(activity.getApplicationContext(), 13),
                        Gravity.RIGHT | Gravity.TOP); // 例如，放在右上角
        GMAdSlotGDTOption.Builder adSlotNativeBuilder = GMAdOptionUtil.getGMAdSlotGDTOption()
                .setNativeAdLogoParams(gdtNativeAdLogoParams);
        /**
         * 创建feed广告请求类型参数AdSlot,具体参数含义参考文档
         * 备注
         * 1: 如果是信息流自渲染广告，设置广告图片期望的图片宽高 ，不能为0
         * 2:如果是信息流模板广告，宽度设置为希望的宽度，高度设置为0(0为高度选择自适应参数)
         */
        GMAdSlotNative adSlot = new GMAdSlotNative.Builder()
                .setGMAdSlotBaiduOption(GMAdOptionUtil.getGMAdSlotBaiduOption().build())//百度相关的配置
                .setGMAdSlotGDTOption(adSlotNativeBuilder.build())//gdt相关的配置
                .setAdmobNativeAdOptions(GMAdOptionUtil.getAdmobNativeAdOptions())//admob相关配置
                .setAdStyleType(AdSlot.TYPE_EXPRESS_AD)//表示请求的模板广告还是原生广告，com.bytedance.msdk.api.AdSlot.TYPE_EXPRESS_AD：模板广告 ； com.bytedance.msdk.api.AdSlot.TYPE_NATIVE_AD：原生广告
                // 备注
                // 1:如果是信息流自渲染广告，设置广告图片期望的图片宽高 ，不能为0
                // 2:如果是信息流模板广告，宽度设置为希望的宽度，高度设置为0(0为高度选择自适应参数)
                .setDownloadType(BFYNewAdMethod.download_type_popup)
                .setImageAdSize(px2dp(getScreenWidth(activity))-distance, 0)// 必选参数 单位dp ，详情见上面备注解释
                .setAdCount(3)//请求广告数量为1到3条
                .build();

        //请求广告，调用feed广告异步请求接口，加载到广告后，拿到广告素材自定义渲染
        /**
         * 注：每次加载信息流广告的时候需要新建一个TTUnifiedNativeAd，否则可能会出现广告填充问题
         * (例如：mTTAdNative = new TTUnifiedNativeAd(this, mAdUnitId);）
         */
        mTTAdNative.loadAd(adSlot, new GMNativeAdLoadCallback() {
            @Override
            public void onAdLoaded(List<GMNativeAd> ads) {

                if (ads == null || ads.isEmpty()) {
                    Log.e(TAG, "on FeedAdLoaded: ad is null!");
                    return;
                }

                for (GMNativeAd ttNativeAd : ads) {
                    Log.e(AppConst.TAG, "   ");
                    Log.e(AppConst.TAG, "adNetworkPlatformId: " + ttNativeAd.getAdNetworkPlatformId() + "   adNetworkRitId：" + ttNativeAd.getAdNetworkRitId() + "   preEcpm: " + ttNativeAd.getPreEcpm());
                }
                GMNativeAd ad = ads.get(0);
                //模板广告特殊处理
                if (ad != null && ad.isExpressAd()) {
                    getExpressAdView(activity,viewGroup,ad,nativeAdCallback);
                    return ;
                }
                // 获取本次waterfall加载中，加载失败的adn错误信息。
                if (mTTAdNative != null)
                    Log.d(TAG, "feed adLoadInfos: " + mTTAdNative.getAdLoadInfoList().toString());
            }

            @Override
            public void onAdLoadedFail(@NonNull AdError adError) {
                Log.e(TAG, "load feed ad error : " + adError.code + ", " + adError.message);
                // 获取本次waterfall加载中，加载失败的adn错误信息。
                if (mTTAdNative != null)
                    Log.d(TAG, "feed adLoadInfos: " + mTTAdNative.getAdLoadInfoList().toString());
                nativeAdCallback.OnError(true,adError.message, adError.code );
            }

        });
    }
    private static int getScreenWidth(Activity activity) {
        DisplayMetrics outMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(outMetrics);
        int widthPixels = outMetrics.widthPixels;
        int heightPixels = outMetrics.heightPixels;
        Log.i("getScreenWidth", "widthPixels = " + widthPixels + " ,heightPixels = " + heightPixels);
        return widthPixels;
    }

    /**
     * Value of px to value of dp.
     *
     * @param pxValue The value of px.
     * @return value of dp
     */
    public static int px2dp(final float pxValue) {
        final float scale = Resources.getSystem().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }


    //渲染模板广告
    @SuppressWarnings("RedundantCast")
    private static void getExpressAdView(final Activity activity,final FrameLayout frameLayout, @NonNull final GMNativeAd ad,final NativeAdCallback nativeAdCallback) {
        final ExpressAdViewHolder adViewHolder;
        try {
            adViewHolder = new ExpressAdViewHolder();
            adViewHolder.mAdContainerView = frameLayout;
            //判断是否存在dislike按钮
            if (ad.hasDislike()) {
                ad.setDislikeCallback(activity, new TTDislikeCallback() {
                    @Override
                    public void onSelected(int position, String value) {
                        //用户选择不喜欢原因后，移除广告展示
                        nativeAdCallback.OnClickDislike();
                        //用户选择不喜欢原因后，移除广告展示
                        frameLayout.removeAllViews();
                    }

                    @Override
                    public void onCancel() {
                        Log.d(TAG, "dislike 点击了取消");
                    }

                    /**
                     * 拒绝再次提交
                     */
                    @Override
                    public void onRefuse() {

                    }

                    @Override
                    public void onShow() {

                    }
                });
            }

            //设置点击展示回调监听
            ad.setNativeAdListener(new GMNativeExpressAdListener() {
                @Override
                public void onAdClick() {
                    Log.d(TAG, "onAdClick");
                    nativeAdCallback.OnClick();
                }

                @Override
                public void onAdShow() {
                    Log.d(TAG, "onAdShow");
                    nativeAdCallback.OnShow(ad.getPreEcpm());
                }

                @Override
                public void onRenderFail(View view, String msg, int code) {
                    Log.d(TAG, "onRenderFail   code=" + code + ",msg=" + msg);
                    nativeAdCallback.OnError(true,msg, code );

                }

                // ** 注意点 ** 不要在广告加载成功回调里进行广告view展示，要在onRenderSucces进行广告view展示，否则会导致广告无法展示。
                @Override
                public void onRenderSuccess(float width, float height) {
                    Log.d(TAG, "onRenderSuccess");
                    //回调渲染成功后将模板布局添加的父View中
                    if (adViewHolder.mAdContainerView != null) {
                        //获取视频播放view,该view SDK内部渲染，在媒体平台可配置视频是否自动播放等设置。
                        int sWidth;
                        int sHeight;
                        /**
                         * 如果存在父布局，需要先从父布局中移除
                         */
                        final View video = ad.getExpressView(); // 获取广告view  如果存在父布局，需要先从父布局中移除
                        if (width == TTAdSize.FULL_WIDTH && height == TTAdSize.AUTO_HEIGHT) {
                            sWidth = FrameLayout.LayoutParams.MATCH_PARENT;
                            sHeight = FrameLayout.LayoutParams.WRAP_CONTENT;
                        } else {
                            sWidth = UIUtils.getScreenWidth(activity);
                            sHeight = (int) ((sWidth * height) / width);
                        }
                        if (video != null) {
                            /**
                             * 如果存在父布局，需要先从父布局中移除
                             */
                            UIUtils.removeFromParent(video);
                            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(sWidth, sHeight);
                            adViewHolder.mAdContainerView.removeAllViews();
                            adViewHolder.mAdContainerView.addView(video, layoutParams);
                        }
                    }
                }
            });


            //视频广告设置播放状态回调（可选）
            ad.setVideoListener(new GMVideoListener() {

                @Override
                public void onVideoStart() {
                    Log.d(TAG, "onVideoStart");
                }

                @Override
                public void onVideoPause() {
                    Log.d(TAG, "onVideoPause");

                }

                @Override
                public void onVideoResume() {
                    Log.d(TAG, "onVideoResume");

                }

                @Override
                public void onVideoCompleted() {
                    Log.d(TAG, "onVideoCompleted");
                }

                @Override
                public void onVideoError(AdError adError) {
                    Log.d(TAG, "onVideoError");
                }
            });
            ad.render();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static class ExpressAdViewHolder {
        FrameLayout mAdContainerView;
    }

    /**
     * 销毁广告，在Activity销毁时调用
     */
    public static void DestroyAd(){
        unregisterConfigCallback();
        if(mTTAdNative!=null)mTTAdNative.destroy();
    }

}
