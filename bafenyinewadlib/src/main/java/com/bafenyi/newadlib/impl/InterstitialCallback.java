package com.bafenyi.newadlib.impl;

public interface InterstitialCallback {

    /**
     *
     * @param ecpm
     * 获取展示广告预估ecpm价格，单位是分 详见：NetworkPlatformConst类
     * AD_NETWORK_NO_PERMISSION = "-3"; //无权限
     * AD_NETWORK_NO_DATA = "-2"; //暂无数据
     * AD_NETWORK_NO_PRICE = "-1"; //平台未有填写的预估ecpm价格
     */
    void onInterstitialSuccess(String ecpm);

    void onInterstitialError(int code, String errorMessage);

    void onInterstitialClose();
}
