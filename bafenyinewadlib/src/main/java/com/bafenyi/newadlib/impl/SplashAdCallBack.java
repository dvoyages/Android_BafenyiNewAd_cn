/*
 * Copyright © 2019 XiaMen BaFenYi Network Technology Co., Ltd. All rights reserved.
 * 版权：厦门八分仪网络科技有限公司版权所有（C）2019
 * 作者：Administrator
 * 创建日期：2020年4月22日
 */

package com.bafenyi.newadlib.impl;

public interface SplashAdCallBack {

    /**
     *
     * @param isNetAvailable  网络是否有效
     * @param message   广告错误信息
     * @param code   错误码
     */
    void OnError(boolean isNetAvailable, String message, int code);

    /**
     * 广告点击
     */
    void OnClick();

    /**
     * 广告展示
     * @param isTT   是否是穿山甲
     * 获取展示广告预估ecpm价格，单位是分 详见：NetworkPlatformConst类
     * AD_NETWORK_NO_PERMISSION = "-3"; //无权限
     * AD_NETWORK_NO_DATA = "-2"; //暂无数据
     * AD_NETWORK_NO_PRICE = "-1"; //平台未有填写的预估ecpm价格
     */
    void OnShow(boolean isTT,String ecpm);

    /**
     *  广告播放完成或者点击跳过
     */
    void skipNextPager();
}
