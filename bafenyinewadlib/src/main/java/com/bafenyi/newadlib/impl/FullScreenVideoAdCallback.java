package com.bafenyi.newadlib.impl;

public interface FullScreenVideoAdCallback {
    /**
     * 广告成功展示
     * 获取展示广告预估ecpm价格，单位是分 详见：NetworkPlatformConst类
     * AD_NETWORK_NO_PERMISSION = "-3"; //无权限
     * AD_NETWORK_NO_DATA = "-2"; //暂无数据
     * AD_NETWORK_NO_PRICE = "-1"; //平台未有填写的预估ecpm价格
     */
    void onRewardSuccessShow(String ecpm);

    /**
     * 关闭广告回调
     */

    void OnClose();


    /**
     *
     * @param isNetAvailable  网络是否有效
     * @param type   错误信息
     * @param code   错误码
     */
    void error(boolean isNetAvailable, String type,int code);
}
