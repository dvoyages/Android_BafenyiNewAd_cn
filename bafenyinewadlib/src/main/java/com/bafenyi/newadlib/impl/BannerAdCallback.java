package com.bafenyi.newadlib.impl;

public interface BannerAdCallback {

    /**
     * banner广告展示
     * 获取展示广告预估ecpm价格，单位是分 详见：NetworkPlatformConst类
     * AD_NETWORK_NO_PERMISSION = "-3"; //无权限
     * AD_NETWORK_NO_DATA = "-2"; //暂无数据
     * AD_NETWORK_NO_PRICE = "-1"; //平台未有填写的预估ecpm价格
     */
    void onShow(String ecpm);

    /**
     * banner广告失败
     */
    void onError(int code, String message);

    /**
     * banner点击
     */
    void onClick();

    /**
     * banner关闭
     */
    void onClose();

}
