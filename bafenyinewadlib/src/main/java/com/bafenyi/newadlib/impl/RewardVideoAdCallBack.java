/*
 * Copyright © 2019 XiaMen BaFenYi Network Technology Co., Ltd. All rights reserved.
 * 版权：厦门八分仪网络科技有限公司版权所有（C）2019
 * 作者：Administrator
 * 创建日期：2020年4月22日
 */

package com.bafenyi.newadlib.impl;

import com.bytedance.msdk.api.reward.RewardItem;

import java.util.Map;

public interface RewardVideoAdCallBack {

    /**
     * 展示激励视频
     * @param isTT   是否是穿山甲
     * 获取展示广告预估ecpm价格，单位是分 详见：NetworkPlatformConst类
     * AD_NETWORK_NO_PERMISSION = "-3"; //无权限
     * AD_NETWORK_NO_DATA = "-2"; //暂无数据
     * AD_NETWORK_NO_PRICE = "-1"; //平台未有填写的预估ecpm价格
     */
    void onShowRewardVideo(boolean isTT,String ecpm);

    /**
     * 关闭视频回调
     *
     * @param rewardVerify 验证奖励是否有效
     */
    void onCloseRewardVideo(boolean rewardVerify);

    /**
     *
     *
     */
    //视频播放完成后，奖励验证回调，rewardVerify：是否有效，rewardAmount：奖励梳理，rewardName：，code：，msg：错误信息
    void onGetReward( boolean rewardVerify, float amount, String rewardName, Map<String, Object> getCustomData);

    /**
     * 错误回调
     *
     * @param isNetAvailable 网络是否有效
     */
    void onErrorRewardVideo(boolean isNetAvailable, String message, int code);


    /**
     * 广告点击
     */
    void onClickRewardVideo();


    /**
     * 广告拉取成功
     */
    void onGetRewardVideo();
}
