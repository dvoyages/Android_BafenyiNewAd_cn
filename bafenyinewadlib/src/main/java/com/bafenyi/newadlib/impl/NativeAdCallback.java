package com.bafenyi.newadlib.impl;

public interface NativeAdCallback {
    /**
     * 广告展示
     * 获取展示广告预估ecpm价格，单位是分 详见：NetworkPlatformConst类
     * AD_NETWORK_NO_PERMISSION = "-3"; //无权限
     * AD_NETWORK_NO_DATA = "-2"; //暂无数据
     * AD_NETWORK_NO_PRICE = "-1"; //平台未有填写的预估ecpm价格
     */
    void OnShow(String ecpm);

    /**
     *
     * @param isNetAvailable  网络是否有效
     * @param message   错误信息
     * @param code   错误码
     */
    void OnError(boolean isNetAvailable, String message,int code);

    /**
     * 广告点击
     */
    void OnClick();

    /**
     * 点击不喜欢
     */
    void OnClickDislike();
}
