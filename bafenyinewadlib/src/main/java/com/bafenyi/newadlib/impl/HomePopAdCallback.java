package com.bafenyi.newadlib.impl;

public interface HomePopAdCallback {

    /**
     * 展示开屏红包弹窗广告
     */
    void onShowHomePopAd();


    /**
     * 拉取激励视频
     */
    void onRequestAd();

    /**
     * 开屏红包弹窗广告拉取视频成功
     * @param isTT  是否穿山甲
     * 获取展示广告预估ecpm价格，单位是分 详见：NetworkPlatformConst类
     * AD_NETWORK_NO_PERMISSION = "-3"; //无权限
     * AD_NETWORK_NO_DATA = "-2"; //暂无数据
     * AD_NETWORK_NO_PRICE = "-1"; //平台未有填写的预估ecpm价格
     */
    void getPopAdSuccess(boolean isTT,String ecpm);

    /**
     * 完成开屏红包弹窗广告
     */
    void onCompleteHomePopAd();

    /**
     * 直接点击关闭
     */

    void onClickClose();


    /**
     * 后台控制不展示
     */

    void onNoShow();
}
