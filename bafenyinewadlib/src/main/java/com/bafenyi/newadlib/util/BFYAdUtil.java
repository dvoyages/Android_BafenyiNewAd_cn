package com.bafenyi.newadlib.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bafenyi.newadlib.BFYNewAdMethod;
import com.bafenyi.newadlib.R;
import com.bafenyi.newadlib.groMoreAd.GroRewardVideoAdUtil;
import com.bafenyi.newadlib.impl.HomePopAdCallback;
import com.bafenyi.newadlib.impl.RewardVideoAdCallBack;
import com.bytedance.msdk.api.reward.RewardItem;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class BFYAdUtil {

    private static Dialog dialog;

    public static boolean isNetworkAvailable(@NonNull Activity context) {
        ConnectivityManager manager = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager == null) {
            return false;
        }
        NetworkInfo networkinfo = manager.getActiveNetworkInfo();
        if (networkinfo == null || !networkinfo.isAvailable()) {
            return false;
        }
        return true;
    }

    public static void saveValue(@NonNull Activity context, @NonNull String key, @NonNull String value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("bfy_ad_sp", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void saveValue(@NonNull Activity context, @NonNull String key, @NonNull boolean value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("bfy_ad_sp", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static String getValue(@NonNull Activity context, @NonNull String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("bfy_ad_sp", Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, "");
    }

    public static String getCurrentTime() {
        Date currentTime = new Date(System.currentTimeMillis());
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd");
        return formatter.format(currentTime);
    }

    public static void showLoadingDialog(@NonNull Activity activity) {
        if (!isActivityAlive(activity)) {
            return;
        }
        dialog = new Dialog(activity, R.style.AppTheme_DownloadDialog);
        dialog.setContentView(R.layout.dialog_bfy_ad_loading);
        dialog.setCancelable(false);
        dialog.show();
    }

    public static boolean isActivityAlive(final Activity activity) {
        return activity != null && !activity.isFinishing()
                && (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1 || !activity.isDestroyed());
    }

    public static void dismissLoadingDialog() {
        if (dialog == null) {
            return;
        }
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    private static boolean isShowRewardVideoAd;
    private static int tryCount;
    public static void playVideoAd(@NonNull final Activity activity,
                                   @NonNull final String adJson,
                                   @NonNull final Dialog dialog,
                                   @Nullable final HomePopAdCallback homePopAdCallback) {
        showLoadingDialog(activity);
        homePopAdCallback.onRequestAd();
        isShowRewardVideoAd=false;
        BFYNewAdMethod.showRewardVideoAd(activity, adJson, new RewardVideoAdCallBack() {


            @Override
            public void onShowRewardVideo(boolean isTT, String ecpm) {
                isShowRewardVideoAd=true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dismissLoadingDialog();
                    }
                }, 200);
                if (homePopAdCallback != null) {
                    homePopAdCallback.getPopAdSuccess(isTT,ecpm);
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        BFYAdUtil.saveValue(activity, "bfy_current_date", BFYAdUtil.getCurrentTime());
                    }
                }, 1000);
            }

            @Override
            public void onCloseRewardVideo(boolean rewardVerify) {
                dismissLoadingDialog();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                if(isShowRewardVideoAd){
                    if (homePopAdCallback != null) {
                        homePopAdCallback.onCompleteHomePopAd();
                    }
                }

            }

            @Override
            public void onGetReward(boolean rewardVerify, float amount, String rewardName, Map<String, Object> getCustomData) {

            }


            @Override
            public void onErrorRewardVideo(boolean isNetAvailable, String message, int code) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dismissLoadingDialog();
                    }
                }, 200);

                if (isNetAvailable) {
                    return;
                }
                if (tryCount >= 1) {
                    tryCount = 0;
                    isShowRewardVideoAd = false;
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    if (homePopAdCallback != null) {
                        homePopAdCallback.onCompleteHomePopAd();
                    }
                } else {
                    Toast.makeText(activity, "加载失败，请重试", Toast.LENGTH_LONG).show();
                    tryCount++;
                }
            }

            @Override
            public void onClickRewardVideo() {

            }

            @Override
            public void onGetRewardVideo() {

            }
        });

    }
}
