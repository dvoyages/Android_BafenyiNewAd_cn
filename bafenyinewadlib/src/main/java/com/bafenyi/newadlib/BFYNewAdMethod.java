package com.bafenyi.newadlib;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bafenyi.newadlib.config.TTAdManagerHolder;
import com.bafenyi.newadlib.groMoreAd.GroBannerAdUtil;
import com.bafenyi.newadlib.groMoreAd.GroFullScreenVideoAdUtil;
import com.bafenyi.newadlib.groMoreAd.GroInterstitialADUtil;
import com.bafenyi.newadlib.groMoreAd.GroNativeAdUtil;
import com.bafenyi.newadlib.groMoreAd.GroRewardVideoAdUtil;
import com.bafenyi.newadlib.groMoreAd.GroSplashAdUtil;
import com.bafenyi.newadlib.impl.BannerAdCallback;
import com.bafenyi.newadlib.impl.FullScreenVideoAdCallback;
import com.bafenyi.newadlib.impl.HomePopAdCallback;
import com.bafenyi.newadlib.impl.InterstitialCallback;
import com.bafenyi.newadlib.impl.NativeAdCallback;
import com.bafenyi.newadlib.impl.RewardVideoAdCallBack;
import com.bafenyi.newadlib.impl.SplashAdCallBack;
import com.bafenyi.newadlib.util.BFYAdUtil;
import com.bytedance.sdk.openadsdk.TTAdConstant;

import org.json.JSONException;
import org.json.JSONObject;

public class BFYNewAdMethod {
    
    public static final String gro_id = "gro_id";
    public static final String gro_splash_id = "gro_splash_id";
    public static final String gro_video_id = "gro_video_id";
    public static final String gro_banner_id = "gro_banner_id";
    public static final String gro_insert_id = "gro_insert_id";
    public static final String gro_native_id = "gro_native_id";
    public static final String gro_full_video_id = "gro_full_video_id";

    private static final String TAG = "BFYNewAdMethod";

    private static boolean isTtInit;  //是否已经初始化



    public static int download_type_popup= TTAdConstant.DOWNLOAD_TYPE_POPUP;//穿山甲是否展示二次弹窗
    public static int download_button=TTAdConstant.SPLASH_BUTTON_TYPE_FULL_SCREEN;//穿山甲开屏全屏按钮点击



    /**
     * @return 返回穿山甲广告是否初始化成功
     */
    public static boolean isTTInit() {
        return isTtInit;
    }

    /**
     * 初始化groMore
     *
     * @param context     上下文
     * @param adAppName   广告名称
     * @param isLocalJson 是否是本地adJson
     * @param adJson      广告id
     * @param isDebugMode 是否是debug模式
     */
    public static void initAd(@NonNull final Context context,
                              @NonNull final String adAppName,
                              boolean isLocalJson,
                              @NonNull String adJson,
                              final boolean isDebugMode) {
        boolean isTtEmpty = !TextUtils.isEmpty(parseJson(gro_id, adJson)) && parseJson(gro_id, adJson).equals("null");
        if (isTtEmpty) {
            Log.i(TAG, "initAd: 广告id不存在！");
            return;
        }
        if (!isLocalJson) {
            //开屏初始化
            if (!isTtInit) {
                TTAdManagerHolder.init(context, parseJson(gro_id, adJson), adAppName, isDebugMode);
                isTtInit = true;
            }
            return;
        }
        //Application初始化
        if (!isTtInit && (!TextUtils.isEmpty(parseJson(gro_id, adJson)) && !parseJson(gro_id, adJson).equals("null"))) {
            //强烈建议在应用对应的Application#onCreate()方法中调用，避免出现content为null的异常
            TTAdManagerHolder.init(context, parseJson(gro_id, adJson), adAppName, isDebugMode);
            isTtInit = true;
        }
    }

    /**
     * 是否展示二次确认弹窗     on/off
     * @return    TTAdConstant.DOWNLOAD_TYPE_NO_POPUP:不做特殊处理; TTAdConstant.DOWNLOAD_TYPE_POPUP：每次下载都触发弹窗
     */
    public static void IsShowDownloadTypePopup(String open){
        if(open!=null&&!open.equals("")){
            if(open.equals("off")){
                download_type_popup= TTAdConstant.DOWNLOAD_TYPE_NO_POPUP;
            }else {
                download_type_popup= TTAdConstant.DOWNLOAD_TYPE_POPUP;
            }
        }else {
            download_type_popup= TTAdConstant.DOWNLOAD_TYPE_POPUP;
        }
    }


    /**
     * 是否全屏    on/off   on：全屏
     * @return    TTAdConstant.SPLASH_BUTTON_TYPE_FULL_SCREEN:全屏响应; TTAdConstant.SPLASH_BUTTON_TYPE_DOWNLOAD_BAR：按钮点击
     */
    public static void IsSplashFullScreenClick(String open){
        if(open!=null&&!open.equals("")){
            if(open.equals("off")){
                download_button=TTAdConstant.SPLASH_BUTTON_TYPE_DOWNLOAD_BAR;
            }else {
                download_button=TTAdConstant.SPLASH_BUTTON_TYPE_FULL_SCREEN;
            }
        }else {
            download_button=TTAdConstant.SPLASH_BUTTON_TYPE_FULL_SCREEN;
        }
    }





    /**
     * 展示开屏广告
     *
     * @param context          上下文
     * @param splashContainer  承载广告View
     * @param adJson           广告Id
     * @param adResultCallBack 广告回调
     */
    public static void showSplashAd(@NonNull final Activity context,
                                    @NonNull final ViewGroup splashContainer,
                                    @NonNull String adJson,
                                    @NonNull final SplashAdCallBack adResultCallBack) {
        if (!isTtInit) {
            Log.i("init_ad_error", "广告没有初始化");
            adResultCallBack.skipNextPager();
            adResultCallBack.OnError(false, "", -999);
            return;
        }
        if (!BFYAdUtil.isNetworkAvailable(context)) {
            Log.i("showSplashAd", "请检查网络是否连接");
            adResultCallBack.OnError(false, "", -999);
            adResultCallBack.skipNextPager();
            return;
        }
        GroSplashAdUtil.showSplashAd(context, parseJson(gro_splash_id, adJson), splashContainer, adResultCallBack);
    }


    /**
     * 展示激励视频广告
     *
     * @param context               上下文
     * @param adJson                大航海广告 json 字符串
     * @param rewardVideoAdCallBack 广告回调
     */
    public static void showRewardVideoAd(@NonNull Activity context,
                                         @NonNull String adJson,
                                         @NonNull RewardVideoAdCallBack rewardVideoAdCallBack) {
        showRewardVideoAd(context, TTAdConstant.VERTICAL, adJson, rewardVideoAdCallBack);
    }

    /**
     * 展示激励视频广告
     *
     * @param context               上下文
     * @param orientation           期望视频的播放方向：TTAdConstant.HORIZONTAL 或 TTAdConstant.VERTICAL
     * @param adJson                大航海广告 json 字符串
     * @param rewardVideoAdCallBack 广告回调
     */
    public static void showRewardVideoAd(@NonNull Activity context,
                                         int orientation,
                                         @NonNull String adJson,
                                         @NonNull RewardVideoAdCallBack rewardVideoAdCallBack) {
        if (!isTtInit) {
            Log.i("init_ad_error", "广告没有初始化");
            rewardVideoAdCallBack.onCloseRewardVideo(false);
            rewardVideoAdCallBack.onErrorRewardVideo(false, "", -999);
            return;
        }
        if (!BFYAdUtil.isNetworkAvailable(context)) {
            Toast.makeText(context, "请检查网络是否连接", Toast.LENGTH_LONG).show();
            rewardVideoAdCallBack.onErrorRewardVideo(false, "", -999);
            return;
        }
        GroRewardVideoAdUtil.showAdWithCallback(context, parseJson(gro_video_id, adJson), orientation, rewardVideoAdCallBack);
    }


    /**
     * 展示banner广告,默认宽高比例,选择宽高类型
     *
     * @param context    上下文
     * @param adJson     大航海广告 json 字符串
     * @param flBannerAd 承载View
     * @param sizeType    TTAdSize.BANNER_CUSTOME/BANNER_320_50等大小
     */
    public static void showBannerAd(@NonNull Activity context,
                                    @NonNull String adJson,
                                    @NonNull ViewGroup flBannerAd,
                                    @NonNull int sizeType) {
        showBannerAd(context, adJson, flBannerAd, sizeType,null);
    }


    /**
     * 展示banner广告,默认宽高比例,选择宽高类型
     *
     * @param context          上下文
     * @param adJson           大航海广告 json 字符串
     * @param flBannerAd       承载View
     * @param sizeType         TTAdSize.BANNER_CUSTOME
     * @param bannerAdCallback banner广告回调
     */
    public static void showBannerAd(@NonNull Activity context,
                                    @NonNull String adJson,
                                    @NonNull ViewGroup flBannerAd,
                                    @NonNull int sizeType,
                                    @Nullable BannerAdCallback bannerAdCallback) {
        if (!isTtInit) {
            Log.i("init_ad_error", "广告没有初始化");
            if (bannerAdCallback != null) {
                bannerAdCallback.onError(-999, "广告没有初始化");
            }
            return;
        }
        GroBannerAdUtil.showBannerAdWithCallback(context, parseJson(gro_banner_id, adJson), flBannerAd, sizeType, bannerAdCallback);
    }


    /**
     * 展示banner广告,自定义宽高
     *
     * @param context    上下文
     * @param adJson     大航海广告 json 字符串
     * @param flBannerAd 承载View
     */
    public static void showBannerAd(@NonNull Activity context,
                                    @NonNull String adJson,
                                    @NonNull ViewGroup flBannerAd,
                                    int width,
                                    int height) {
        showBannerAd(context, adJson, flBannerAd, width, height, null);
    }


    /**
     * 展示banner广告,自定义宽高
     *
     * @param context          上下文
     * @param adJson           大航海广告 json 字符串
     * @param flBannerAd       承载View
     * @param bannerAdCallback banner广告回调
     */
    public static void showBannerAd(@NonNull Activity context,
                                    @NonNull String adJson,
                                    @NonNull ViewGroup flBannerAd,
                                    int width,
                                    int height,
                                    @Nullable BannerAdCallback bannerAdCallback) {
        if (!isTtInit) {
            Log.i("init_ad_error", "广告没有初始化");
            if (bannerAdCallback != null) {
                bannerAdCallback.onError(-999, "广告没有初始化");
            }
            return;
        }
        GroBannerAdUtil.showBannerAdWithCallback(context, parseJson(gro_banner_id, adJson), flBannerAd, width, height, bannerAdCallback);
    }


    /**
     * 展示全屏（插屏）广告
     *
     * @param context              上下文
     * @param adJson               大航海广告 json 字符串
     * @param weight               尺寸宽
     * @param height            尺寸高
     * @param interstitialCallback 插屏回调
     */
    public static void showInterstitialAd(@NonNull Activity context,
                                          @NonNull String adJson,
                                          int weight,
                                          int height ,
                                          @NonNull InterstitialCallback interstitialCallback) {
        if (!isTtInit) {
            Log.i("init_ad_error", "广告没有初始化");
            interstitialCallback.onInterstitialError(999, "广告未初始化");
            return;
        }
        GroInterstitialADUtil.showInterstitialAdWithCallBack(context, parseJson(gro_insert_id, adJson), weight, height, interstitialCallback);

    }


    /**
     * 展示原生广告
     *
     * @param context   上下文
     * @param adJson    大航海广告 json 字符串
     * @param viewGroup 承载View
     * @param distance  穿山甲两边到屏幕两边的距离总和  全屏传入0 单位dp
     */
    public static void showNativeAd(@NonNull Activity context,
                                    @NonNull String adJson,
                                    @NonNull FrameLayout viewGroup,
                                    @NonNull int distance,
                                    @NonNull NativeAdCallback nativeAdCallback) {
        if (!isTtInit) {
            Log.i("init_ad_error", "广告没有初始化");
            nativeAdCallback.OnError(false, "", -999);
            return;
        }
        GroNativeAdUtil.showNativeAdWithCallback(context, parseJson(gro_native_id, adJson),viewGroup,distance,nativeAdCallback);
    }




    /**
     * 展示全屏视频广告/新插屏
     *
     * @param context 上下文
     * @param adJson  大航海广告 json 字符串
     */
    public static void showFullScreenVideoAd(@NonNull Activity context,
                                             @NonNull String adJson,
                                             @NonNull FullScreenVideoAdCallback fullScreenVideoAdCallback) {
        if ( !isTtInit) {
            Log.i("init_ad_error", "广告没有初始化");
            fullScreenVideoAdCallback.error(false, "", -999);
            return;
        }
        GroFullScreenVideoAdUtil.showAdWithCallback(context, parseJson(gro_full_video_id, adJson), fullScreenVideoAdCallback);
    }
    /**
     * 解析json
     */
    public static String parseJson(String key, String adJson) {
        if (TextUtils.isEmpty(key) || TextUtils.isEmpty(adJson)) {
            return "null";
        }
        try {
            JSONObject jsonObject = new JSONObject(adJson);
            String parseValue = jsonObject.getString(key);
            return TextUtils.isEmpty(parseValue) ? "null" : parseValue;
        } catch (JSONException e) {
            e.printStackTrace();
            return "null";
        }
    }
}
